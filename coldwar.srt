﻿1
00:01:08,965 --> 00:01:12,580
♪ <i>I knocked. I cried. ♪
♪ She wouldn't open up</i> ♪

2
00:01:16,507 --> 00:01:20,157
♪ <i>So I had to put my little head</i>
<i>Down on the stone</i> ♪

3
00:01:38,632 --> 00:01:42,069
♪ <i>Head on stone,</i>
<i>Feet on the threshold</i> ♪

4
00:01:46,257 --> 00:01:49,694
♪ <i>Open up my love,</i>
<i>for the fear of God</i> ♪

5
00:02:02,726 --> 00:02:06,299
{\fs40}COLD WAR

6
00:02:07,757 --> 00:02:11,917
You're not afraid
it's too monotonous and primitive?

7
00:02:12,204 --> 00:02:13,880
No, why?

8
00:02:13,906 --> 00:02:16,608
Where I come from
every drunk guy sing like this.

9
00:02:31,233 --> 00:02:34,065
♪ <i>Oh, Dana, oh Dana,</i>
<i>I won't marry a master</i> ♪

10
00:02:34,340 --> 00:02:37,173
♪ <i>I will marry someone of my own ilk</i> ♪

11
00:03:06,757 --> 00:03:09,838
♪ <i>I won't go drinking with you,</i>
<i>Because I'll only regret</i> ♪

12
00:03:25,424 --> 00:03:30,212
♪ <i>Two hearts, four eyes</i> ♪

13
00:03:32,793 --> 00:03:37,983
♪ <i>Crying all day and all night</i> ♪

14
00:03:41,091 --> 00:03:46,210
♪ <i>Dark eyes, you cry beacuse</i> ♪

15
00:03:46,424 --> 00:03:51,462
♪ <i>We can't be together</i> ♪

16
00:04:04,380 --> 00:04:07,652
{\fs30}Poland, 1949

17
00:04:26,216 --> 00:04:28,361
This is beatiful.

18
00:04:28,591 --> 00:04:32,028
- What langauage are they singing in?
- Lemko.

19
00:04:33,495 --> 00:04:35,355
I thought so.

20
00:04:38,430 --> 00:04:40,291
Shame.

21
00:04:40,424 --> 00:04:43,957
- Why?
- That it's not ours.

22
00:04:45,080 --> 00:04:48,646
Mr. Kaczmarek, whether it's ours
or not is none of your business.

23
00:06:34,133 --> 00:06:37,214
Dear youth!
You have the right to ask,

24
00:06:37,522 --> 00:06:40,383
what are we doing here in front of
the palace of landowners.

25
00:06:40,509 --> 00:06:44,077
I have a duty to answer:
Now this is your home.

26
00:06:44,154 --> 00:06:48,367
And these door leads
to the world of music, song and dance.

27
00:06:49,035 --> 00:06:52,160
The music that was born
in the field of slavery.

28
00:06:52,267 --> 00:06:54,756
The music of your grandparents
and great grandfathers.

29
00:06:54,782 --> 00:06:57,614
The music of pain, harm and humiliation.

30
00:06:58,257 --> 00:07:00,912
If it was uplifting,
it was always through tears.

31
00:07:01,312 --> 00:07:05,044
Now you have the right to ask if
all of you could enter this place.

32
00:07:05,200 --> 00:07:08,104
And my answer would be:
No, defintely not everyone.

33
00:07:08,422 --> 00:07:11,500
Only the best of the
best will come inside,

34
00:07:11,526 --> 00:07:14,263
and under careful
eyes of our teachers,

35
00:07:14,675 --> 00:07:17,923
they will enter the stages of
Poland and the entire block.

36
00:07:18,157 --> 00:07:22,038
That's why I am challaneging you to
a ruthless and noble fight

37
00:07:22,175 --> 00:07:24,035
with each other and with yourselves.

38
00:07:24,299 --> 00:07:27,570
This is a start of a new era for
our folk musicians.

39
00:07:27,758 --> 00:07:29,583
- Hurray!
- Hurray!

40
00:07:52,425 --> 00:07:55,993
- Do you think they want us to read music?
- They want it 'the peasant way'.

41
00:08:02,467 --> 00:08:04,529
And you?
What will you sing?

42
00:08:04,686 --> 00:08:06,546
Something from highlands.

43
00:08:08,252 --> 00:08:09,958
And you?

44
00:08:11,383 --> 00:08:13,244
I do not know yet.

45
00:08:14,381 --> 00:08:16,526
Sing your song for us.

46
00:08:19,426 --> 00:08:23,027
♪ <i>I'm over the water,</i> ♪
♪ <i>you're over the water too,</i> ♪

47
00:08:23,258 --> 00:08:27,003
♪ <i>How can I give you my hand</i> ♪

48
00:08:27,330 --> 00:08:29,274
♪ <i>I'd give it to you, and...</i> ♪

49
00:08:29,300 --> 00:08:31,291
Nice. I know it.

50
00:08:33,190 --> 00:08:35,643
It works for two voices.

51
00:08:36,693 --> 00:08:41,185
♪ <i>I'm over the water,</i> ♪
♪ <i>you're over the water, too</i> ♪

52
00:08:41,467 --> 00:08:45,912
♪ <i>We can't be together</i> ♪

53
00:08:46,092 --> 00:08:49,707
♪ <i>So let's teach a horse to swim</i> ♪

54
00:08:49,753 --> 00:08:53,024
♪ <i>So we could be together</i> ♪

55
00:08:53,425 --> 00:08:56,696
♪ <i>Let's teach a horse to swim</i> ♪

56
00:08:57,015 --> 00:09:00,027
♪ <i>So we could be together</i> ♪

57
00:09:00,053 --> 00:09:01,647
- Thanks a lot.
- Thank you.

58
00:09:01,673 --> 00:09:04,446
Excuse me...
The girl with the fringe...

59
00:09:04,675 --> 00:09:07,164
Could you do something alone?

60
00:09:07,425 --> 00:09:09,796
- Sing?
- Yes.

61
00:09:33,177 --> 00:09:34,642
- Thank you.
- Just the chorus.

62
00:10:01,184 --> 00:10:03,424
- Where did you get it from?
- From a film.

63
00:10:03,918 --> 00:10:07,347
A mobile cinema came to our village
and they showed a Russian film.

64
00:10:07,467 --> 00:10:09,627
It was called "Jolly Fellows"
and that song was in it.

65
00:10:09,674 --> 00:10:12,826
- And what is it about?
- About love! "Heart".

66
00:10:14,240 --> 00:10:15,966
And how's your dancing?

67
00:10:16,001 --> 00:10:18,490
I know the basic steps,
and I can learn the rest.

68
00:10:18,516 --> 00:10:21,239
- That's great. Thank you.
- Thank you.

69
00:10:25,141 --> 00:10:27,630
That other girl had
a beautifully pure voice.

70
00:10:27,670 --> 00:10:30,538
- Yes, but this one got something else.
- What is it?

71
00:10:30,793 --> 00:10:34,159
Energy, attitude.
She is very original.

72
00:10:35,551 --> 00:10:37,506
If you say so.

73
00:10:43,359 --> 00:10:46,683
We should be able to
find 12 that can dance.

74
00:10:50,454 --> 00:10:52,522
Can you see that yours one?

75
00:10:54,051 --> 00:10:55,911
What do you mean "mine"?

76
00:10:56,092 --> 00:10:58,616
The one who sang "Heart".

77
00:10:58,948 --> 00:11:01,010
She is onto something...

78
00:11:04,714 --> 00:11:06,362
Look...

79
00:11:07,468 --> 00:11:09,495
Cheeky girl.

80
00:11:10,884 --> 00:11:14,407
And not even from highlands, but
central Poland. Little liar.

81
00:11:14,782 --> 00:11:17,972
- She has a good voice though.
- And been to prison already.

82
00:11:18,493 --> 00:11:19,912
How come?

83
00:11:19,938 --> 00:11:22,060
- Apparently she killed her father.
- What?

84
00:11:22,174 --> 00:11:25,326
Supposedly.
Still have suspended sentence.

85
00:11:29,582 --> 00:11:32,983
- How do you know that?
- Kaczmarek checked it.

86
00:11:42,093 --> 00:11:44,155
Your head, Zula!

87
00:12:14,009 --> 00:12:16,214
Heads up.

88
00:12:25,181 --> 00:12:27,078
Attach it properly.

89
00:12:27,835 --> 00:12:29,729
- Fuck!
- Fuck!

90
00:12:44,596 --> 00:12:46,800
What really happend to the father?

91
00:12:47,725 --> 00:12:50,842
- What father?
- Your father.

92
00:12:51,172 --> 00:12:52,923
What was it supposed to be?

93
00:12:53,280 --> 00:12:55,271
Why were you in jail?

94
00:12:57,122 --> 00:13:00,467
He had mistaken me with my mother, so
I showed him the difference with a knife.

95
00:13:02,908 --> 00:13:05,301
He survived, don't be afraid.

96
00:13:11,436 --> 00:13:15,414
Are you interested in me, because
I have a talent or in general?

97
00:13:18,885 --> 00:13:20,318
Try this one.

98
00:14:00,264 --> 00:14:02,103
Very good...

99
00:15:10,552 --> 00:15:13,740
Pull, pull your leg higher!!

100
00:15:15,635 --> 00:15:17,211
Good!

101
00:15:25,802 --> 00:15:27,947
Beautiful Mateusz, bravo!

102
00:15:35,969 --> 00:15:37,959
Hold the pace!

103
00:15:43,553 --> 00:15:46,457
Look at each other!

104
00:15:46,727 --> 00:15:50,057
One, two, three, one, two, three,
twist and turn!

105
00:15:50,302 --> 00:15:52,957
One, two, three, one, two, three!

106
00:16:23,333 --> 00:16:26,775
{\fs30}Warsaw, 1951

107
00:16:28,121 --> 00:16:32,743
♪ <i>Two hearts, four eyes</i> ♪

108
00:16:37,927 --> 00:16:42,870
♪ <i>Cried all day and all night</i> ♪

109
00:16:48,844 --> 00:16:53,904
♪ <i>Dark eyes, you cry beacuse</i> ♪

110
00:16:54,136 --> 00:17:00,452
♪ <i>We can't be together</i> ♪

111
00:17:00,677 --> 00:17:08,535
♪ <i>We can't be together</i> ♪

112
00:17:20,969 --> 00:17:25,496
♪ <i>My mother told me</i> ♪

113
00:17:30,927 --> 00:17:35,657
♪ <i>you musn't fall in love with this boy</i> ♪

114
00:17:41,750 --> 00:17:46,621
♪ <i>Made of stone
my heart should have been</i> ♪

115
00:17:46,761 --> 00:17:53,742
♪ <i>so that boy wouldn't have fallen for me</i> ♪

116
00:18:37,928 --> 00:18:41,578
I've had enough of this.
It's starting to irritate me.

117
00:18:42,761 --> 00:18:44,622
Come on, we're getting off.

118
00:18:44,844 --> 00:18:48,661
- Wait, let me enjoy it.
- Let them have fun.

119
00:18:50,524 --> 00:18:52,800
We will have
some fun on our own.

120
00:18:58,975 --> 00:19:01,197
I must tell you something.

121
00:19:01,443 --> 00:19:05,722
Honestly, I never believed
in all of this, in all this folk art.

122
00:19:05,928 --> 00:19:09,294
And this... touched me deeply.
You are a real genius!

123
00:19:09,511 --> 00:19:13,197
To make something so beautiful
out of something like that?

124
00:19:14,442 --> 00:19:16,185
Thank you both.

125
00:19:17,307 --> 00:19:20,555
This is the most
beautiful day of my life.

126
00:20:20,387 --> 00:20:23,964
You've uncovered some priceless
treasures of our national culture.

127
00:20:23,990 --> 00:20:26,229
It is beautiful and respectable.

128
00:20:27,200 --> 00:20:32,522
Your ensamble have a great potential.
I'd like this to become our main showcase.

129
00:20:33,239 --> 00:20:37,510
But I think it's time to introduce some
new elements to the repertoire,

130
00:20:37,887 --> 00:20:42,924
something about the land reform,
global peace and its threats.

131
00:20:43,970 --> 00:20:48,149
A strong piece about
the leader of global proletariat.

132
00:20:48,678 --> 00:20:52,382
We will appreciate it,
and reward it.

133
00:20:54,609 --> 00:20:59,799
In the future, who knows, maybe you could
visit Berlin, Prague, Budapest, Moscow..?

134
00:20:59,928 --> 00:21:02,204
What do you think about it?

135
00:21:05,178 --> 00:21:09,670
On behalf of the whole "Mazurek",
thank you for appreciating our work,

136
00:21:09,696 --> 00:21:14,164
but the repertoire is based
on the authentic folk art.

137
00:21:14,367 --> 00:21:19,359
They don't sing
about reforms, peace and leaders.

138
00:21:19,714 --> 00:21:22,221
They just don't do that.
So it would be difficult.

139
00:21:22,904 --> 00:21:25,109
I understand.

140
00:21:25,574 --> 00:21:30,293
If you don't mind... My name is
Kaczmarek Lech, operations manager.

141
00:21:30,512 --> 00:21:34,486
Comrade Bielecka, I assure
you that our nation

142
00:21:34,512 --> 00:21:37,700
isn't naive at all. That
including the rural communities.

143
00:21:37,726 --> 00:21:41,922
On the contrary. They will sing
about these most important issues,

144
00:21:42,247 --> 00:21:44,700
according to their hidden needs,

145
00:21:45,166 --> 00:21:48,692
should we give them permission,
and necessary support.

146
00:21:49,701 --> 00:21:52,908
This is where I see
the importance of "Mazurek".

147
00:21:54,116 --> 00:21:55,348
Thank you.

148
00:22:10,070 --> 00:22:11,638
Thank you all.

149
00:22:11,664 --> 00:22:16,784
♪ <i>...About our wise and beloved</i> ♪

150
00:22:17,054 --> 00:22:21,996
♪ <i>by all nations - Stalin</i> ♪

151
00:22:22,220 --> 00:22:27,091
♪ <i>The beatiful songs written</i> ♪

152
00:22:27,304 --> 00:22:32,424
♪ <i>throughout this century</i> ♪

153
00:22:32,637 --> 00:22:37,698
♪ <i>About our wise and beloved</i> ♪

154
00:22:37,929 --> 00:22:42,907
♪ <i>by all nations - Stalin</i> ♪

155
00:22:43,095 --> 00:22:48,074
♪ <i>The beatiful songs written</i> ♪

156
00:22:48,304 --> 00:22:54,503
♪ <i>throughout this century</i> ♪

157
00:22:54,721 --> 00:23:02,044
♪ <i>About our wise and beloved</i> ♪

158
00:23:25,429 --> 00:23:27,488
We vow to you, our Motherland...

159
00:23:30,387 --> 00:23:33,674
for the will of great
patriots and revolutionaries...

160
00:23:36,494 --> 00:23:39,595
Feliks Dzierzynski and
Karol Świerczewski...

161
00:23:40,101 --> 00:23:42,340
We're going to Berlin.

162
00:23:44,971 --> 00:23:47,838
For the youth festival in December.

163
00:23:50,044 --> 00:23:54,168
Today Berlin, tomorrow Moscow, and the
day after tomorrow God knows where!

164
00:23:54,455 --> 00:23:58,070
...and to develop everlasting friendship

165
00:23:58,096 --> 00:24:00,549
with a powerful Soviet state...

166
00:24:00,762 --> 00:24:04,579
Don't you think something
must be done with this Janicka?

167
00:24:04,804 --> 00:24:08,052
- What about her?
- She is too dark.

168
00:24:10,096 --> 00:24:14,469
This ensamble is supposed to be
Polish folk, typically Slavic.

169
00:24:16,388 --> 00:24:18,911
Just look at her eyes.

170
00:24:22,429 --> 00:24:25,759
How do you know about Berlin?

171
00:24:26,052 --> 00:24:28,459
From the minister.

172
00:24:33,804 --> 00:24:37,300
Okay, maybe I'm exaggerating.
We can always dye her.

173
00:24:37,326 --> 00:24:43,810
♪ <i>He who was nothing
will become everything,</i> ♪

174
00:24:44,221 --> 00:24:48,250
♪ <i>This is our final
and decisive battle</i> ♪

175
00:24:59,402 --> 00:25:03,052
I will always be with you, everywhere
and until the end of the world.

176
00:25:10,093 --> 00:25:12,795
But I have to tell you something.

177
00:25:13,721 --> 00:25:15,427
Hm?

178
00:25:19,576 --> 00:25:21,816
I'm grassing on you.

179
00:25:30,012 --> 00:25:32,536
What does it mean?
"Grassing"?

180
00:25:32,924 --> 00:25:36,457
I go to Kaczmarek every week to confess.

181
00:25:40,452 --> 00:25:43,700
But I am not saying anything
that would harm you.

182
00:25:45,763 --> 00:25:49,331
The worst thing is
that he is fancies me.

183
00:25:50,436 --> 00:25:52,047
What is he asking you about?

184
00:25:52,221 --> 00:25:56,631
What were you up to during the war,
do you listen to Radio Free Europe...

185
00:25:56,846 --> 00:25:59,368
do you have any dollars,
do you believe in God...

186
00:26:00,051 --> 00:26:01,630
And do you believe?

187
00:26:02,069 --> 00:26:03,527
I do.

188
00:26:08,263 --> 00:26:11,664
I knew it will be like this.
Damn me, idiot.

189
00:26:12,878 --> 00:26:16,737
What would you do in my place?
I have a suspended sentence.

190
00:26:16,763 --> 00:26:18,908
Otherwise he wouldn't have let me in.

191
00:26:19,096 --> 00:26:20,744
Wiktor!

192
00:26:22,672 --> 00:26:26,963
Ok, get lost you fucking posh boy!
Don't need you anymore!

193
00:26:28,846 --> 00:26:32,379
If I wanted to fuck you up,
I would have done it already!

194
00:28:07,680 --> 00:28:10,821
Today Berlin is, as you
know, the front line

195
00:28:10,847 --> 00:28:14,556
between the socialist camp
and the imperialist camp.

196
00:28:14,582 --> 00:28:17,699
Between the forces of peace
and the revisionists.

197
00:28:18,222 --> 00:28:23,165
And on the front line, like always,
strange things may happen.

198
00:28:23,372 --> 00:28:28,776
That is why we must have our eyes open
be vigilant and responsible for each other.

199
00:28:28,972 --> 00:28:31,994
And besides, what can I tell you..

200
00:28:32,222 --> 00:28:34,533
Germans will always be Germans.

201
00:28:35,022 --> 00:28:38,113
And that's why, this is happening
for the first and only time,

202
00:28:38,139 --> 00:28:40,201
Cheers to the adventure of your life!

203
00:28:44,264 --> 00:28:45,496
Excuse me.

204
00:28:47,097 --> 00:28:48,424
Excuse me.

205
00:29:23,305 --> 00:29:26,196
400 meters take about ten minutes to walk.

206
00:29:26,222 --> 00:29:29,837
The Russian sector ends here.
And I will be waiting here.

207
00:29:44,681 --> 00:29:48,295
But what will I do over there?
Who will I be?

208
00:29:51,270 --> 00:29:54,766
You will be there with me.
We'll be together.

209
00:29:54,792 --> 00:29:57,447
I don't speak French or anything else.

210
00:29:57,765 --> 00:30:01,261
You will learn it.
You have a good ear. You have a talent.

211
00:30:02,429 --> 00:30:06,328
You are unique.
I don't want to live without you.

212
00:30:15,472 --> 00:30:17,925
Ok. Let's do it.

213
00:31:47,112 --> 00:31:52,552
What's up with you? There is a party.
The comrades want to meet you.

214
00:31:52,723 --> 00:31:55,745
Let me do my hair, okay?
I'll be there shortly.

215
00:31:58,426 --> 00:32:00,208
You look nice.

216
00:32:01,905 --> 00:32:03,593
Hurry up.

217
00:32:43,806 --> 00:32:47,930
- Poland, and East Germany..
- Poland, Germany...

218
00:32:48,140 --> 00:32:50,416
Good friendship.

219
00:32:50,640 --> 00:32:52,595
Communist brothers.

220
00:32:52,806 --> 00:32:56,421
He is referring to
communist friendship.

221
00:32:56,640 --> 00:32:59,828
We are brothers
Brothers and sisters.

222
00:33:00,015 --> 00:33:03,203
We are brothers and sisters.

223
00:33:05,890 --> 00:33:07,990
- Does it taste good?
- Yes.

224
00:33:08,015 --> 00:33:10,160
- It's homemade.
- Really?

225
00:33:10,390 --> 00:33:12,132
It is our specialty.

226
00:33:12,348 --> 00:33:14,742
Thank you. It's very good.

227
00:33:14,973 --> 00:33:18,054
- The fish is from the Baltic Sea.
- Really?

228
00:34:18,890 --> 00:34:20,419
Excuse me.

229
00:35:40,879 --> 00:35:43,328
{\fs30}Paris, 1954

230
00:36:02,849 --> 00:36:04,211
Taxi!

231
00:36:25,682 --> 00:36:27,245
Same again, please.

232
00:36:27,271 --> 00:36:30,206
I'm sorry, but we're really closing now.

233
00:36:30,315 --> 00:36:33,847
You can go to the " Chez Marlette",
if you want. They are open till 1am.

234
00:36:34,391 --> 00:36:39,428
But between us, since she
hasn't come yet, she won't come at all.

235
00:36:44,766 --> 00:36:45,807
Ah!

236
00:37:03,904 --> 00:37:06,305
How long will you be in Paris for?

237
00:37:06,682 --> 00:37:08,543
We're going back tomorrow.

238
00:37:10,312 --> 00:37:12,329
How are you doing in here?

239
00:37:14,432 --> 00:37:16,035
Fine thank you.

240
00:37:16,324 --> 00:37:19,856
I arrange, compose, play in a club.

241
00:37:21,754 --> 00:37:23,579
Generally very good.

242
00:37:26,412 --> 00:37:27,869
Are you with somebody?

243
00:37:28,888 --> 00:37:30,422
Yes.

244
00:37:30,797 --> 00:37:32,409
Me too.

245
00:37:35,349 --> 00:37:37,910
And? Are you happy?

246
00:37:47,579 --> 00:37:51,388
- I'll take you to your hotel.
- Better not. You know how things are.

247
00:37:51,509 --> 00:37:53,287
Just a little bit.

248
00:37:53,699 --> 00:37:55,951
Nobody will see us.

249
00:38:00,308 --> 00:38:05,001
Could you tell me why you
didn't come that day?

250
00:38:10,264 --> 00:38:12,919
I felt we wouldn't make it.

251
00:38:15,031 --> 00:38:18,279
Not that we won't be able to escape...

252
00:38:18,433 --> 00:38:21,549
it just wasn't for me.
I felt worse.

253
00:38:21,766 --> 00:38:23,793
What do you mean "worse"?

254
00:38:23,974 --> 00:38:25,930
Worse than you...

255
00:38:26,141 --> 00:38:28,381
Worse in general.

256
00:38:28,941 --> 00:38:31,123
- You know what I mean.
- I don't.

257
00:38:31,471 --> 00:38:33,873
I know love is love, that's it.

258
00:38:34,024 --> 00:38:37,923
I know one thing.
I wouldn't run away without you.

259
00:38:39,558 --> 00:38:41,383
Here is fine.

260
00:39:55,850 --> 00:39:58,931
Been with whores?

261
00:40:00,364 --> 00:40:02,983
I can't afford whores.

262
00:40:04,603 --> 00:40:07,402
I was with the woman of my life.

263
00:40:07,981 --> 00:40:09,972
Wonderful.

264
00:40:10,975 --> 00:40:13,286
Well, let me sleep then.

265
00:40:39,219 --> 00:40:43,068
{\fs30}Yugoslavia, 1955

266
00:41:24,531 --> 00:41:27,248
It looks alright now, doesn't it?

267
00:41:27,812 --> 00:41:29,586
- Good morning.
- Good morning.

268
00:41:29,684 --> 00:41:31,639
- Do you have a ticket?
- Yes.

269
00:41:31,850 --> 00:41:34,588
I am very curious about what you'll say.

270
00:41:34,760 --> 00:41:39,467
New reportiare, new songs,
one even from Yugoslavia.

271
00:41:39,850 --> 00:41:42,162
As a nod to our hosts.

272
00:41:42,384 --> 00:41:43,960
Nice.

273
00:41:44,184 --> 00:41:47,585
In general, it has now gained
momentum, and fame.

274
00:41:48,050 --> 00:41:51,700
It is a pity you weren't there.
You had a real talent.

275
00:41:52,809 --> 00:41:54,339
Come and join us in our box.

276
00:41:54,365 --> 00:41:56,853
No, thanks.
I have a very good place.

277
00:41:57,086 --> 00:41:59,645
Good. Good.
See you later.

278
00:42:06,392 --> 00:42:13,847
♪ <i>Silver thread on Serbian cloth</i> ♪

279
00:42:14,017 --> 00:42:21,211
♪ <i>Thin yarn, fine stitches,</i>
<i>says my beloved sweetheart</i> ♪

280
00:42:21,463 --> 00:42:25,048
♪ <i>Milan...</i> ♪

281
00:42:25,267 --> 00:42:28,953
♪ <i>My dearest</i> ♪

282
00:42:29,086 --> 00:42:36,197
♪ <i>sweethert from Moravia</i> ♪

283
00:42:36,517 --> 00:42:44,517
♪ <i>I promised my hand to Milan</i> ♪

284
00:42:45,934 --> 00:42:51,433
<i>Silk yarn, threaded with my dreams</i> ♪

285
00:42:51,642 --> 00:42:57,580
<i>Fine thread made out my dreams
soon to be broken</i> ♪

286
00:44:27,777 --> 00:44:29,981
Where are you taking me?

287
00:44:30,226 --> 00:44:32,317
To Moscow.

288
00:44:33,018 --> 00:44:36,085
But why?
I'm here legally. I have a visa.

289
00:44:36,184 --> 00:44:38,983
In France, I am a resident.
I'm not Polish anymore.

290
00:44:39,009 --> 00:44:40,659
We know all of that.

291
00:44:42,773 --> 00:44:45,712
Relax, you're not going to Moscow.

292
00:44:46,684 --> 00:44:51,473
You are going to Zagreb, then
to Paris or wherever you want.

293
00:44:53,592 --> 00:44:55,513
The Polish wanted us to
send you back to Warsaw.

294
00:44:55,538 --> 00:44:57,931
But it's not in your intrest, isn't it?

295
00:44:59,256 --> 00:45:00,477
Fuck.

296
00:45:12,143 --> 00:45:15,508
- Let me stay. One night.
- The train is waiting.

297
00:45:15,726 --> 00:45:20,783
Please understand,
this is about a woman of my life...

298
00:45:20,810 --> 00:45:23,487
So what? Do you want to come
back to Warsaw with her?

299
00:45:24,224 --> 00:45:26,002
"Femme fatale".

300
00:45:29,099 --> 00:45:32,429
They say that Warsaw
is the Paris of the East.

301
00:45:35,060 --> 00:45:38,923
♪ <i>My mother forbade me</i> ♪

302
00:45:44,601 --> 00:45:49,259
♪ <i>to fall in love with this boy</i> ♪

303
00:45:55,563 --> 00:45:59,988
♪ <i>but I will cacth him anyway</i> ♪

304
00:46:00,014 --> 00:46:07,182
♪ <i>and will love him as long as I live</i> ♪

305
00:46:07,533 --> 00:46:13,725
♪ <i>and will love him as long as I live</i> ♪

306
00:47:09,956 --> 00:47:13,210
{\fs30}Paris, 1957

307
00:47:17,852 --> 00:47:21,100
Hold on!
The piano doesn't fit in here.

308
00:47:21,261 --> 00:47:24,034
We will use it only in the murder scene.

309
00:47:24,179 --> 00:47:28,741
The strings came too early.
Wait for the shadow to appear.

310
00:47:29,125 --> 00:47:30,239
Ok.

311
00:48:05,685 --> 00:48:08,625
- So you have a husband, right?
- Yes.

312
00:48:10,702 --> 00:48:12,847
I did it for us.

313
00:48:14,083 --> 00:48:17,413
It wasn't a church wedding,
so it doesn't count.

314
00:48:18,394 --> 00:48:21,368
- What's your name now?
- Gangarossa-Lichon.

315
00:48:21,394 --> 00:48:25,387
- Ganga-what?
- Gangarossa, It's a Sicilian surname.

316
00:48:26,727 --> 00:48:29,453
What's important is
that you are not married.

317
00:48:29,769 --> 00:48:32,424
- Are you?
- Come on!

318
00:48:39,379 --> 00:48:41,524
I've been waiting for you.

319
00:51:09,228 --> 00:51:13,091
♪ <i>Two hearts, four eyes</i> ♪

320
00:51:18,436 --> 00:51:22,300
♪ <i>Crying all day and all night</i> ♪

321
00:51:27,644 --> 00:51:32,171
♪ <i>Dark eyes, you cry beacuse</i> ♪

322
00:51:32,394 --> 00:51:36,388
♪ <i>We can't be together</i> ♪

323
00:51:36,603 --> 00:51:41,972
♪ <i>We can't be together</i> ♪

324
00:51:51,019 --> 00:51:54,136
♪ <i>My mother told me</i> ♪

325
00:51:59,978 --> 00:52:04,256
♪ <i>you musn't fall in love with this boy</i> ♪

326
00:52:09,686 --> 00:52:14,213
♪ <i>But I went for him anyway</i> ♪

327
00:52:14,436 --> 00:52:18,004
♪ <i>And will love him till the end</i> ♪

328
00:52:18,228 --> 00:52:23,206
♪ <i>And will love him till the end</i> ♪

329
00:52:55,268 --> 00:52:57,669
Aren't there too many "r" in a row?

330
00:52:57,872 --> 00:52:59,069
No.

331
00:53:08,784 --> 00:53:11,178
It doesn't fit
with the melody either.

332
00:53:12,603 --> 00:53:15,476
If you sing it right, it will fit.

333
00:53:17,228 --> 00:53:20,309
The meaning is wrong as well, isn't it?

334
00:53:21,978 --> 00:53:24,633
It's just a free translation.

335
00:53:24,747 --> 00:53:28,813
I wonder who translated it?
Don't say that it was your lady poet.

336
00:53:30,437 --> 00:53:34,928
Yes, so what?
Juliette did it for us for free.

337
00:53:35,336 --> 00:53:37,041
I hope so.

338
00:53:37,612 --> 00:53:39,474
What a stupid text:

339
00:53:39,645 --> 00:53:44,054
"A pendulum killed the time."
"Oh my God". Yeah, oh my God!

340
00:53:44,430 --> 00:53:48,874
Since when you are a critic? Juliette is a
very good poet. Very famous too.

341
00:53:49,711 --> 00:53:52,806
- They just published her poetry book.
- I won't sing this.

342
00:53:52,833 --> 00:53:56,103
- Ok, you won't get the record then.
- So I won't get the record then.

343
00:54:31,187 --> 00:54:34,268
And be nice to Michel,
he can do a lot of things here.

344
00:54:34,767 --> 00:54:37,726
Somehow we like each other,
and you'll like you even more.

345
00:54:37,937 --> 00:54:40,475
- Why more?
- Because you're cute.

346
00:54:40,786 --> 00:54:42,931
You have, what they call<i> A slavic charm.</i>

347
00:54:42,982 --> 00:54:44,991
Don't go overboard with
your makeup, you know...

348
00:54:45,017 --> 00:54:47,263
- Why?
- It's good enough as it is.

349
00:54:49,190 --> 00:54:51,845
And be yourself, don't get uptight.

350
00:55:00,104 --> 00:55:03,126
- With or without a tie?
- With a tie.

351
00:55:06,421 --> 00:55:09,917
- Good evening.
- Good evening.

352
00:55:10,187 --> 00:55:13,435
- Who will be there?
- Some interesting people

353
00:55:13,645 --> 00:55:16,301
- What about your outstanding lady poet?
- Most likely.

354
00:55:16,520 --> 00:55:20,016
Come on, she is harmless.
She already had two lovers since me.

355
00:55:20,261 --> 00:55:22,880
As for Michel, you will
know yourself where you are with him.

356
00:55:24,437 --> 00:55:27,139
Good evening.

357
00:55:34,812 --> 00:55:37,644
- Hey, Wiktor!
- This is Michel.

358
00:55:38,691 --> 00:55:40,509
This is Michel. This is Zula.

359
00:55:40,647 --> 00:55:45,458
Nice to meet you. I was longing
to meet you. I've heard a lot about you.

360
00:55:45,770 --> 00:55:47,572
- Nice to hear that, thank you.
- You are welcome.

361
00:55:47,581 --> 00:55:49,085
How did the movie come out?

362
00:55:49,110 --> 00:55:53,252
Wonderful. We are pleased.
It's also a great news for you.

363
00:55:53,479 --> 00:55:55,287
- I am pleased.

364
00:55:55,312 --> 00:55:59,176
You two carry on,
I will have a look around.

365
00:55:59,541 --> 00:56:01,686
She's a character.

366
00:56:28,286 --> 00:56:30,739
Good evening.

367
00:56:30,896 --> 00:56:32,602
Excuse me. Good evening.

368
00:56:32,860 --> 00:56:36,889
- I was thinking a lot about your text.
- Yeah? Which one?

369
00:56:37,112 --> 00:56:39,723
- To my song.
- Ah, yes.

370
00:56:40,202 --> 00:56:43,870
"A pendulum killed time."
That's nice, but I don't understand it.

371
00:56:43,896 --> 00:56:46,598
Really? It's a metaphor.

372
00:56:46,812 --> 00:56:48,555
Meaning what?

373
00:56:49,812 --> 00:56:53,676
That time doesn't matter
when you are in love.

374
00:56:59,549 --> 00:57:01,594
Do you like it in here?

375
00:57:01,674 --> 00:57:03,878
In Paris? It's ok.

376
00:57:05,573 --> 00:57:08,092
It must have been
quite a shock to you.

377
00:57:08,373 --> 00:57:10,138
Shock? Why?

378
00:57:10,256 --> 00:57:14,155
Cinemas, cafes, restaurants,
shops, etc...

379
00:57:15,935 --> 00:57:18,117
Honestly speaking, I had
much better life in Poland.

380
00:57:18,142 --> 00:57:20,354
Really?
Then why did you run away?

381
00:57:20,435 --> 00:57:24,553
I didn't run away. I married
an Italian. I left legally.

382
00:57:26,944 --> 00:57:29,466
Have you ever been to Palermo?

383
00:57:29,688 --> 00:57:30,838
No.

384
00:57:31,904 --> 00:57:33,303
What a pity.

385
00:57:35,729 --> 00:57:37,698
It was nice to meet you.

386
00:57:53,688 --> 00:57:55,145
Excuse me.

387
00:57:57,001 --> 00:57:58,925
What the fuck did you tell him?

388
00:57:59,104 --> 00:58:02,455
- About what?
- That I'd pretended to be from highlands.

389
00:58:02,481 --> 00:58:05,394
That I was grassing on you.
That I killed my father.

390
00:58:07,231 --> 00:58:09,119
That I danced for
Stalin in the Kremlin.

391
00:58:09,145 --> 00:58:11,172
- But that's really cool.
- Yeah? And why is that?

392
00:58:11,198 --> 00:58:14,138
- Because I wanted to highlight you.
- Highlight me?

393
00:58:14,979 --> 00:58:17,919
- This is how things work in here.
- How's that?

394
00:58:18,788 --> 00:58:19,952
I don't know.

395
00:58:20,106 --> 00:58:24,643
Edith Piaf worked in a brothel,
and people love her for that.

396
00:58:24,990 --> 00:58:27,207
What brothel?
What are you doing to me?

397
00:58:27,438 --> 00:58:30,686
And Giovanni wasn't an aristocrat,
he was selling crystals.

398
00:58:31,771 --> 00:58:35,706
Fine, I won't say a thing.
Can we go home now?

399
00:58:36,771 --> 00:58:39,046
I met your mistress.

400
00:58:39,730 --> 00:58:43,173
Nice. Good looking, but...

401
00:58:43,199 --> 00:58:46,289
..a bit old.
But you probably suit each other well.

402
00:58:55,253 --> 00:58:57,315
<i>A metaphor!</i>

403
00:58:57,569 --> 00:58:58,904
What an idiot.

404
00:59:07,146 --> 00:59:08,971
He is a jerk.

405
00:59:10,855 --> 00:59:12,561
So be it.

406
00:59:16,730 --> 00:59:18,869
Zula, don't you worry.

407
00:59:19,641 --> 00:59:21,668
Whatever will be, will be.

408
00:59:25,313 --> 00:59:27,376
I love him though.

409
00:59:29,188 --> 00:59:32,459
Zula, are you alright?

410
00:59:41,435 --> 00:59:45,094
- What are you doing here?
- Come in, I just felt sad.

411
00:59:45,398 --> 00:59:48,561
Not now, honey.
We're going to "L'Eclipse".

412
00:59:48,706 --> 00:59:52,320
- Everyone is coming.
- Ok, give me a moment.

413
01:01:50,980 --> 01:01:54,915
In Poland, you were something.
In here you're different.

414
01:02:04,522 --> 01:02:08,683
No, maybe I'm just silly.
I probably just imagined it.

415
01:02:17,199 --> 01:02:20,797
Other than that, it's great.
We're in Paris...

416
01:02:20,980 --> 01:02:23,541
I love you with all my strengths.

417
01:02:23,649 --> 01:02:27,140
And when it comes to Michel,
don't worry about him at all.

418
01:02:27,166 --> 01:02:30,532
We have an understaning.
I'll sort everything out.

419
01:03:19,569 --> 01:03:21,312
Stop!

420
01:03:23,106 --> 01:03:24,468
What?

421
01:03:25,522 --> 01:03:27,170
This is empty.

422
01:03:28,337 --> 01:03:29,534
Empty?

423
01:03:32,147 --> 01:03:35,929
We only got 40 minutes left.
Please, don't waste it for us.

424
01:03:43,064 --> 01:03:46,928
Ok. I'll do it right.
You'll be pleased.

425
01:03:48,403 --> 01:03:50,358
It is not about me.

426
01:03:51,828 --> 01:03:54,767
Everything in here is for you.

427
01:03:55,813 --> 01:03:56,952
Believe in yourself.

428
01:03:56,978 --> 01:04:00,166
I believe in myself.
I don't believe in you.

429
01:04:03,468 --> 01:04:05,175
Can we?

430
01:04:06,064 --> 01:04:08,209
Start over.

431
01:05:30,440 --> 01:05:32,467
Why are you so sad?

432
01:05:34,773 --> 01:05:37,442
Look, our first child.

433
01:05:38,401 --> 01:05:40,013
Bastard.

434
01:05:43,422 --> 01:05:45,033
What happened?

435
01:05:46,065 --> 01:05:47,925
Nothing in particular.

436
01:05:48,981 --> 01:05:50,937
It was amazing.

437
01:05:52,065 --> 01:05:54,127
Them French know what they are doing.

438
01:05:54,291 --> 01:05:58,669
And Michel is an absoltute master.
He fucked me six times in one night.

439
01:06:00,146 --> 01:06:02,258
Unlike a Polish artist in exile.

440
01:06:07,914 --> 01:06:10,059
This I can understand.

441
01:06:50,398 --> 01:06:51,922
Come out!

442
01:06:51,948 --> 01:06:53,809
Excuse me.

443
01:06:55,786 --> 01:06:59,111
- What did you do with her?
- Nothing. She went back.

444
01:06:59,898 --> 01:07:01,676
To Poland.

445
01:07:02,982 --> 01:07:05,293
- To Poland?
- Yes, to Poland.

446
01:08:34,982 --> 01:08:36,309
Hello?

447
01:08:37,357 --> 01:08:39,881
Is this "Mazurek"?

448
01:08:40,940 --> 01:08:43,180
"Mazurek", in Bialaczow.

449
01:08:44,232 --> 01:08:46,093
0418.

450
01:08:49,107 --> 01:08:51,170
Can I speak with
Zuzanna Lichon, please?

451
01:08:52,232 --> 01:08:53,429
Lichon.

452
01:08:54,531 --> 01:08:55,858
Zula.

453
01:08:59,774 --> 01:09:01,729
She just came back.

454
01:09:04,982 --> 01:09:06,381
I understand.

455
01:09:07,816 --> 01:09:10,209
Don't you know where she is?

456
01:09:38,149 --> 01:09:41,089
I don't know how
we could help you.

457
01:09:42,149 --> 01:09:46,048
You are not a Frenchman.
And you're not Polish anymore.

458
01:09:46,330 --> 01:09:50,265
As for us, for Polish
authorities, you don't exist.

459
01:09:51,691 --> 01:09:56,751
But honestly, why are you
trying to go back?

460
01:09:56,982 --> 01:09:58,558
I'm Polish.

461
01:09:59,681 --> 01:10:01,780
- Please give me a break.
- But I am.

462
01:10:01,806 --> 01:10:05,303
You're not Polish. You'd
escaped, betrayed us, and lied about us.

463
01:10:05,329 --> 01:10:07,922
You'd left
young people who trusted you.

464
01:10:08,552 --> 01:10:10,520
You don't love Poland.

465
01:10:10,796 --> 01:10:13,190
- I do.
- No, you don't.

466
01:10:20,548 --> 01:10:23,167
But there is a solution.

467
01:10:26,024 --> 01:10:28,612
If you sincerely regret your actions...

468
01:10:30,076 --> 01:10:35,179
You have a special position
in the artistic circles of Paris, right?

469
01:10:37,245 --> 01:10:39,805
You know people from different enviorments.

470
01:11:13,666 --> 01:11:16,685
{\fs30}Poland, 1959

471
01:11:27,483 --> 01:11:32,638
♪ <i>What does this remind you of?</i> ♪

472
01:11:33,281 --> 01:11:39,136
♪ <i>What does this remind you of?</i> ♪
♪ <i>Such a familiar feeling again!</i> ♪

473
01:11:48,275 --> 01:11:49,922
Morning.

474
01:12:14,681 --> 01:12:16,803
You look terrible.

475
01:12:23,650 --> 01:12:25,226
How long?

476
01:12:26,816 --> 01:12:28,563
15.

477
01:12:31,816 --> 01:12:34,305
Could have been worse apparently.

478
01:12:35,136 --> 01:12:37,162
I crossed the
border illegally twice

479
01:12:37,188 --> 01:12:39,682
and I was spying for the British.

480
01:12:57,025 --> 01:12:59,170
You have 10 minutes.

481
01:13:20,525 --> 01:13:22,801
What have we done?

482
01:13:33,990 --> 01:13:36,847
- I'll wait for you.
- Stop it.

483
01:13:37,165 --> 01:13:40,809
Find youself a normal guy.
Somebody who can live with you.

484
01:13:43,858 --> 01:13:46,596
He wasn't born yet.

485
01:13:54,866 --> 01:13:56,948
I'll get you out of here.

486
01:13:59,483 --> 01:14:03,171
♪ <i>Bayo bongo, bayo bongo</i> ♪

487
01:14:03,400 --> 01:14:06,758
♪ <i>Bayo bongo, o bongo bayay</i> ♪

488
01:14:07,192 --> 01:14:10,877
♪ <i>Bayo bongo, bayo bongo</i> ♪

489
01:14:11,067 --> 01:14:14,681
♪ <i>Through the whole wide world</i>
<i>this song goes to you</i> ♪

490
01:14:14,818 --> 01:14:18,089
♪ <i>Bayo bongo, bayo bongo</i> ♪

491
01:14:18,400 --> 01:14:21,766
♪ <i>Bayo bongo, o bongo bayay</i> ♪

492
01:14:21,984 --> 01:14:25,977
♪ <i>Who knows this beautiful dance?</i>
<i>Will dance with me now</i> ♪

493
01:14:26,192 --> 01:14:29,380
♪ <i>Oh, bayo bongo,</i>
<i>Oh, bongo Bayay</i> ♪

494
01:14:55,525 --> 01:14:59,270
It's nice of you to find the time.
Come on Piotrus, don't be sad.

495
01:14:59,484 --> 01:15:01,474
Introduce yourself to mister...

496
01:15:02,609 --> 01:15:04,469
Piotrus.

497
01:15:04,692 --> 01:15:06,470
Hi.

498
01:15:08,359 --> 01:15:09,888
He's shy.

499
01:15:12,650 --> 01:15:15,732
- Does he look like me?
- Spitting image.

500
01:15:17,400 --> 01:15:20,482
Now what? Will you come back to music?

501
01:15:21,525 --> 01:15:24,049
- Not really.
- Yeah.

502
01:15:25,945 --> 01:15:29,542
I am glad you managed to get
out of there. It wasn't easy.

503
01:15:29,681 --> 01:15:32,529
But the deputy minister is
our neighbor, and a good friend.

504
01:15:32,555 --> 01:15:35,992
Thank you very much.
I'm grateful, really.

505
01:15:40,067 --> 01:15:43,526
I heard your Paris record.
Very nice.

506
01:15:44,021 --> 01:15:46,226
Great arrangements.

507
01:15:47,703 --> 01:15:51,513
It would be great to do it
again, here with us, in Polish.

508
01:15:51,734 --> 01:15:55,002
Zula would surely need that.
You too, by the way.

509
01:15:58,151 --> 01:16:00,079
Look, mummy's coming.

510
01:16:14,692 --> 01:16:18,093
I love you dearly, but I have to throw up.

511
01:16:19,940 --> 01:16:21,843
Stay at the banquet.

512
01:16:21,962 --> 01:16:25,292
There will be a lot of interesting
people from the world of art.

513
01:16:25,886 --> 01:16:28,008
Come on, come on...

514
01:16:45,066 --> 01:16:47,057
Take me away from here.

515
01:16:48,156 --> 01:16:49,957
That's why I am here.

516
01:16:50,438 --> 01:16:52,269
But this time forever.

517
01:18:59,774 --> 01:19:01,670
Repeat after me..

518
01:19:03,365 --> 01:19:06,847
I, Wiktor Warski, I am taking you,
Zuzanna Lichon, as my wife...

519
01:19:06,873 --> 01:19:10,363
I, Wiktor Warski, I am taking you,
Zuzanna Lichon, as my wife...

520
01:19:10,887 --> 01:19:14,421
and I promise I will never leave you
until death do us part.

521
01:19:15,276 --> 01:19:19,361
And I promise I will never leave you
until death do us part.

522
01:19:20,269 --> 01:19:23,329
I, Zuzanna Lichon, take you,
Wiktor Warski, as my husband

523
01:19:23,545 --> 01:19:26,983
and I promise I will never leave you
until death do us part.

524
01:19:28,087 --> 01:19:30,042
So help us God.

525
01:19:40,418 --> 01:19:43,037
You have more as you are heavier.

526
01:19:54,860 --> 01:19:56,773
I am yours now.

527
01:19:59,532 --> 01:20:01,654
Forever and ever.

528
01:20:41,860 --> 01:20:44,313
Let's go to the other side.

529
01:20:47,693 --> 01:20:50,633
The view is better there.

530
01:21:02,605 --> 01:21:06,335
"To my parents"

531
01:23:18,151 --> 01:23:21,276
English subs by:
rollingpeople

