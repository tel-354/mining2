Sure I won't get in trouble, Bill?
Don't be a wuss.
I'd come with you if I weren't dying.
You're not dying!
You didn't see the vomit coming out of my nose this morning?
That's disgusting.
Okay. Go get the wax.
In the cellar?
You want it to float, don't you?
Fine.
<i>Georgie.</i>
Hurry up.
Okay. I'm brave.
Where's the wax?
There's the wax.
Yes.
What was that? What's that?
Oh, jeez.
All right.
There you go.
She's all ready, Captain.
She?
You always call boats "she."
"She."
Thanks, Billy.
See you later. Bye.
Be careful.
No!
No!
Oh, Bill's gonna kill me.
Hiya, Georgie.
What a nice boat.
Do you want it back?
Um, yes, please.
You look like a nice boy.
I bet you have a lot of friends.
Three, but my brother's my best best.
Where is he?
In bed. Sick.
I bet I can cheer him up.
I'll give him a balloon.
Do you want a balloon too, Georgie?
I'm not supposed to take stuff from strangers.
Oh... Well, I'm Pennywise the Dancing Clown.
"Pennywise?" "Yes." "Meet Georgie.
"Georgie, meet Pennywise."
Now we aren't strangers, are we?
What are you doing in the sewer?
A storm blew me away.
Blew the whole circus away.
Can you smell the circus, Georgie?
There's peanuts, cotton candy,
<i>hot dogs,</i> and...
Popcorn?
Popcorn! Is that your favorite?
- Uh-huh.
- Mine, too.
Because they pop. Pop, pop, pop.
Pop, pop, pop.
Pop.
I should get going now.
Oh.
Without your boat?
<i>You don't wanna lose it, Georgie.</i>
Bill's gonna kill you.
Here.
Take it.
Take it, Georgie.
Help!
Billy!
<i>Pull it, Mike.</i>
<i>Go on now, pull it.</i>
Here, reload it.
You need to start taking more responsibility around here, Mike.
Your dad was younger than you when he took...
I'm not my dad, okay?
Yeah.
Look at me, son.
Look at me!
There are two places you can be in this world.
You can be out here like us or you can be in there, like them.
You waste time hemming and hawing, and someone else is gonna make that choice for you.
Except you won't know it until you feel that bolt between your eyes.
So, there's this church full of Jews, right?
And Stan has to take this super Jewy test.
But how's it work?
They slice the tip of his dick off.
But then Stan will have nothing left!
- That's true.
- Wait up, you guys!
Hey, Stan, what happens at the Bar Mitzvah, anyways?
Ed says they slice the tip of your dick off.
Yeah, and I think the rabbi's gonna pull down your pants, turn to the crowd and say,
"Where's the beef?"
At the Bar Mitzvah, I read from the Torah, and then I make a speech and suddenly, I become a man.
I could think of funner ways to become a man.
- <i>"More fun" you mean.</i>
- Oh, shit.
Think they'll sign my yearbook?
"Dear Richie, sorry for taking a hot, steaming dump in your backpack last March.
<i>"Have a good summer."</i>
<i>Are you in there by yourself, Beaver-ly?</i>
Or do you have half the guys in the school with you, huh, slut?
I know you're in there, little shit.
I can smell you.
<i>No wonder you don't have any friends.</i>
Which is it, Gretta?
Am I a slut or a little shit?
Make up your mind.
You're trash.
We just wanted to remind you.
<i>Such a loser.</i>
<i>Well, at least now you'll smell better.</i>
<i>Gross.</i>
Let's go, girls.
<i>Have a nice summer, Beaver-ly.</i>
<i>Pathetic.</i>
Best feeling ever.
Yeah?
Try tickling your pickle for the first time.
Hey, what do you guys wanna do tomorrow?
I start my training.
- Wait, what training?
- <i>Street Fighter.</i>
Is that how you wanna spend your summer?
Inside of an arcade?
Beats spending it inside of your mother.
- Oh!
- What if we go to the quarry?
Guys, we have the Barrens.
Right.
Betty Ripsom's mom.
<i>Is she really expecting to see her come out of that school?</i>
I don't know.
As if Betty Ripsom's been hiding in Home Ec. for the last few weeks.
You think they'll actually find her?
Sure.
In a ditch. All decomposed, covered in worms and maggots.
Smelling like Eddie's mom's underwear.
Shut up! That's freaking disgusting.
She's not dead. She's missing.
Sorry, Bill. She's missing.
You know the Barrens aren't that bad.
Who doesn't love splashing around in shitty water?
Nice Frisbee, flamer.
Give it back!
Fucking losers!
Loser.
You suck, Bowers.
Shut up, Bill.
You say something, Billy?
You got a free ride this year
'cause of your little brother.
Ride's over, Denbrough.
This summer's gonna be a hurt train for you and your faggot friends.
I wish he'd go missing.
He's probably the one doing it.
You gonna let me go by?
Or is there a secret password or something?
Oh.
- Um, sorry.
- Sorry isn't...
A password.
Henry and his goons are over by the west entrance.
So you should be fine.
Oh, I wasn't...
Everyone knows he's looking for you.
What you listening to?
New Kids on the Block.
I don't even like them. I was just...
Wait. You're the new kid, right?
Now I get it.
There's nothing to get.
I'm just messing with you.
I'm Beverly Marsh.
Yeah. I know that
'cause we're in the same class.
Social Studies. And you were...
I'm Ben. But pretty much everybody just calls me...
The new kid.
Well, Ben, there are worse things to be called.
Let me sign this.
Stay cool, Ben from sosh class.
Uh, yeah.
You too, Beverly.
Hang tough, new kid on the block.
"Please Don't Go, Girl."
That's the name of another
New Kids on the Block song.
"He thrusts his fists against the posts...
"He thrusts his fists against...
Shit!
"Post."
Need some help? I...
I thought we agreed.
- Before you say anything...
- Bill.
Just let me show you something first.
The Barrens.
It's the only place that
Georgie could have ended up.
He's gone, Bill.
But if the storm swept Georgie in, we should have gone...
He's gone! He's dead!
<i>He's dead! There is nothing we can do!
Nothing!</i>
Now take this down before your mother sees it.
Next time you wanna take something from my office...
Ask.
I guess you get your tunnels back.
Get in!
<i>Creep!</i>
Oh, Jesus.
<i>Mike!</i>
<i>Hurry, son!</i>
<i>Help! It burns!</i>
<i>Mike!</i>
Stay the fuck outta my town!
Mike?
<i>Are you okay?</i>
You're not studying, Stanley.
How's it going to look?
The rabbi's son can't finish his own Torah reading.
Take the book to my office.
<i>Obviously you're not using it.</i>
<i>Take everything but the Delicious Deals, guys.</i>
<i>My mom loves them.</i>
Hey! First you said the Barrens, and now you're saying the sewer.
I mean, what if we get caught?
We won't, Eds. The sewers are public works.
We're the public, aren't we?
Hey, Eddie, these your birth control pills?
Yeah, and I'm saving it for your sister.
This is private stuff.
<i>Hello, and welcome to</i>
<i>the</i> Derry Children's Hour.
Eddie Bear, where you boys off to in such a rush?
Um...
Just my backyard, Mrs. K.
I got a new...
- A new croquet set.
- Jeez, spit it out, Bill!
Okay.
Oh, and sweetie, don't go rolling around on the grass.
Especially if it's just been cut.
You know how bad your allergies can get.
Yes, Mom. Let's go.
<i>Aren't you forgetting something?</i>
<i>Water is all around us. It's even above us.</i>
<i>Don't believe me?</i>
<i>Then try it yourself at home.</i>
<i>Whenever you're outside...</i>
Do you want one from me too, Mrs. K?
- I was kidding.
- No, no, no.
Sorry, Mommy.
"Your hair is winter fire, January embers...
"My heart burns there too."
<i>Slow down!</i>
<i>Hi-ho, Silver!</i>
Away!
Your old lady bike's too fast for us!
Found it.
Isn't it summer vacation?
I would think you'd be ready to take a break from the books.
I like it in here.
A boy should be spending his summer outside with friends.
Don't you have any friends?
Can I have the book now?
<i>Egg boy.</i>
<i>What on earth are you doing?</i>
Where are you off to, tits?
Gotcha.
<i>Wait.</i>
- Fucking hold him.
- Leave me alone!
- Smack him.
- Don't let tubby get away.
Help!
Get him!
- Hold him, Hockstetter.
- Get him, Belch.
Stop!
- Just leave me alone.
- Look at all this blubber!
Let me light his hair, like Michael Jackson.
Just hold him.
Get off me! Get off me!
Help!
Help!
Okay, new kid.
This is what us locals call the Kissing Bridge.
It's famous for two things. Sucking face, and carving names.
Henry, please.
Whoa, whoa! Henry!
Shut up!
I'm gonna carve my whole name onto this cottage cheese!
I'm gonna cut your fucking tits off.
I swear to God!
Get him!
<i>Come on!</i>
<i>Get him!</i>
You can't run!
- Oh, no!
- <i>We need to find Fatty!</i>
My knife. My old man will kill me!
- You two, get him!
- Come on!
Move your fucking ass!
<i>He's going that way! Come on!</i>
<i>He's down there.</i>
I don't know. I guess.
That's poison ivy. And that's poison ivy.
And that's poison ivy.
Where? Where's the poison ivy?
Nowhere. Not every fucking plant is poison ivy, Stanley.
Okay, I'm starting to get itchy now and I'm pretty sure this is not good for my...
Do you use the same bathroom as your mother?
- Sometimes, yeah.
- Then you probably have crabs.
<i>That's so not funny.</i>
Aren't you guys coming in?
Uh-uh. It's greywater.
What the hell's greywater?
It's basically piss and shit.
So I'm just telling you,
<i>you guys are splashing around in millions of gallons of Derry pee. So...</i>
Are you serious? What are you...
Doesn't smell like caca to me, <i>se�or.</i>
Okay, I can smell that from here.
It's probably just your breath wafting back into your face.
Have you ever heard of a staph infection?
Oh, I'll show you a staff infection.
<i>This is so unsanitary. You're literally...</i>
<i>This is literally like swimming inside of a toilet bowl right now.</i>
<i>Have you ever heard of Listeria?</i>
<i>Are you retarded?</i>
You're the reason why we're in this position right now.
Guys!
Shit. Don't tell me that's...
<i>No.</i>
Georgie wore galoshes.
Whose sneaker is it?
It's Betty Ripsom's.
Oh, shit.
<i>Oh, God. Oh, fuck!</i>
I don't like this.
How do you think Betty feels?
Running around these tunnels with only one frickin' shoe?
What if she's still here?
Eddie, come on!
My mom will have an aneurysm, if she finds out that we're playing down here.
I'm serious.
Bill?
If... If I was Betty Ripsom,
I would want us to find me.
Georgie too.
What if I don't want to find them?
I mean, no offense, Bill, but I don't want to end up like...
I don't want to go missing either.
- He has a point.
- You too?
It's summer.
We're supposed to be having fun.
<i>This isn't fun.</i>
This is scary and disgusting.
Holy shit! What happened to you?
I hear ya, tits.
Don't think you can stay down here all damn day now.
You found us, Patrick.
<i>You found us, Patrick.</i>
<i>Patrick.</i>
Fuck!
I think it's great that we're helping the new kid, but also we need to think of our own safety.
I mean, he's bleeding all over and you guys know that there's an AIDS epidemic happening right now as we speak, right?
My mom's friend in New York City got it by touching a dirty pole on the subway.
And a drop of AIDS blood got into his system through a hangnail. A hangnail!
And you can amputate legs and arms.
But how do you amputate a waist?
You guys do know that alleys are known for dirty needles that have AIDS, right?
You guys do know that?
- Hey...
- Ah, we're screwed.
Richie, wait here. Come on.
Glad I got to meet you before you died.
Okay.
<i>Can we afford all that?</i>
It's all we got.
You kidding me?
Wait, you have an account here, don't you?
If my mom finds out
I bought all this stuff for myself...
I'm spending the whole rest of the weekend in the emergency room getting X-rayed.
- <i>See you later, Dad.</i>
- <i>See you, Gretta.</i>
You okay?
I'm fine. What's wrong with you?
None of your business.
There's a kid outside.
Looked like someone killed him.
We need some supplies, but we don't have enough money.
I like your glasses, Mr. Keene.
You look just like Clark Kent.
Oh.
I don't know about that.
Can I try them?
Mmm. Sure.
What do you think?
Well, how about that?
You look just like Lois Lane.
Really?
Mmm.
Well, here you go.
Shoot, I'm so sorry.
It's okay.
<i>Just suck the wound.</i>
- <i>I need to focus right now.</i>
- <i>You need to focus?</i>
Yeah, can you go get me something?
- Jesus! What do you need?
- Go get my bifocals.
I hid 'em in my second fanny pack.
<i>Why do you have two fanny packs?</i>
<i>I need to focus right now and it's a long story.</i>
<i>I don't want to get into it.</i>
Um, thanks.
Even-steven.
<i>Oh, God, he's bleeding. Oh, my God!</i>
Ben from sosh?
You have to suck the wound before you apply the Band-Aids.
- This is 101.
- You don't know what you're talking about.
Are you okay? That looks like it hurts.
Oh. No, I'm good. I just fell.
Yeah, right into Henry Bowers.
Shut it, Richie.
Why? It's the truth.
You sure they got the right stuff to fix you up?
You know, we'll take care of him.
Uh, thanks again, Beverly.
Sure. Maybe I'll see you around.
Yeah, we were thinking about going to the quarry tomorrow, if you wanna
<i>come.</i>
Good to know. Thanks.
Nice going bringing up Bowers in front of her.
Yeah, dude, you heard what she did.
What'd she do?
More like "Who'd she do?"
From what I hear, the list is longer than my wang.
That's not saying much.
They're just rumors.
Anyway, Bill had her back in third grade.
<i>They kissed in the school play.</i>
<i>The reviews said you can't fake that sort of passion.</i>
Now, pip-pip and tally-ho, my good fellows,
I do believe this chap requires our utmost attention.
Get in there, Dr. K. Come on, fix him up.
Why don't you shut the fuck up,
Einstein, because I know what I'm doing and I don't want you doing the British guy with me right now.
Suck the wound. Get in there.
<i>Toilet and bath water travel down</i>
<i>the drains and into the sewer.</i>
<i>The sewer is a fun place to play with all of your friends.</i>
<i>Just follow the water into the drains and down into the sewers you go.</i>
<i>When you're with your friends in the sewers, you can be as silly as a clown!</i>
<i>That's right, it's the word of the day.</i>
Hi, Daddy.
Hey, Bevvie.
Whatcha got there?
Just some things.
Like what?
Tell me you're still my little girl.
Yes, Daddy.
Good.
<i>A deep fly ball to the left. His last at bat.</i>
<i>He scratched the foul pole.</i>
<i>Wade Boggs, of course, leads the league in hitting so far.</i>
This is what you did. This is what you...
This is...
<i>Lay it out.</i>
Oh, my God.
- So easy.
- Poor tree.
So easy!
Oh, my God, that was terrible. I win.
- You won?
- Yeah.
- Did you see my loogie?
- That went the farthest!
- It's by distance.
- Mass. It's always been mass.
What is mass?
Who cares how far it goes?
It matters how cool it looks, like it's green or it's white or juicy and fat.
- All right.
- Thanks for that.
- Who's first?
- That was terrible.
<i>I'll go!</i>
<i>Sissies.</i>
What the fuck!
Oh, holy shit! We just got showed up by a girl.
Do we have to do that now?
Yes.
<i>Come on!</i>
Oh, shit.
I already won! We already won!
They're down! Yes!
<i>Ah, fuck! What was that?</i>
Something just touched my foot right here.
That work?
- Where are we looking?
- Right here, right here!
It's a turtle.
News flash, Ben. School's out for summa!
Oh, that? That's not school stuff.
- Who sent you this?
- No one. Give it...
No one.
- What's with the history project?
- Oh...
When I first moved here,
I didn't have anyone to hang out with, so I just started spending time in the library.
<i>You went to the library?</i>
On purpose?
Oh, I wanna see.
<i>What's The Black Spot?</i>
<i>The Black Spot was a nightclub</i>
<i>that was burned down years ago by that racist cult.</i>
<i>The what?</i>
<i>Don't you watch</i> Geraldo?
Your hair...
Your... Your hair is beautiful, Beverly.
Oh.
Right. Thanks.
<i>Here, pass it.</i>
Why is it all murders and missing kids?
Derry's not like any town
I've ever been in before.
They did a study once, and it turns out people die or disappear six times the national average.
You read that?
<i>And that's just grown-ups.</i>
Kids are worse.
Way, way worse.
I've got more stuff if you wanna see it.
No, no.
<i>Don't freak out, just tell us.</i>
<i>Yeah. I heard he has a rollercoaster and a pet chimp</i> and an old guy's fucking bones. Yeah.
Whoa, whoa, whoa.
Wow.
Cool, huh?
No. No, nothing cool.
<i>There's nothing cool.</i>
This is cool, right here.
Wait, no. No, it's not cool.
What's that?
Oh, that? That's the charter for Derry Township.
Nerd alert.
No, actually, it's really interesting.
Derry started as a beaver trapping camp.
Still is, am I right, boys?
Ninety-one people signed the charter that made Derry.
<i>But later that winter, they all disappeared without a trace.</i>
The entire camp?
<i>There were rumors of Indians,</i> but no sign of an attack.
<i>Everybody just thought it was a plague or something.</i>
<i>But it's like</i>
<i>one day everybody just woke up and left.</i>
The only clue was a trail of bloody clothes leading to the well house.
<i>Jesus. We can get Derry</i>
<i>on</i> Unsolved Mysteries.
- Let's do that. You're brilliant.
- I might be.
I don't know.
Maybe he's just trying to make some friends, Stanley.
Where was the well house?
I don't know. Somewhere in town, I guess.
<i>Why?</i>
Nothing.
<i>Nobody knows the trouble I've seen</i>
<i>Nobody knows my sorrow</i>
<i>Eddie...</i>
<i>What are you looking for?</i>
Oh!
Fuck. My mom's gonna fucking flip.
Do you think this will help me, Eddie?
Help! Help!
Where you going, Eds?
If you lived here, you'd be home by now.
Come join the clown, Eds.
<i>You'll float down here.</i>
We all float down here.
Yes, we do.
"Your hair is winter fire...
"January embers...
"My heart burns there, too."
<i>Beverly...</i>
<i>Beverly.</i>
<i>Help me.</i>
<i>Help me, please.</i>
<i>We all want to meet you, Beverly.</i>
<i>We all float down here.</i>
Hello? Who are you?
- <i>I'm Veronica.</i>
- <i>Betty Ripsom.</i>
<i>Patrick Hockstetter.</i>
<i>Come closer.</i>
- <i>Wanna see?</i>
- <i>We float.</i>
<i>We change.</i>
<i>Daddy!</i>
Help!
Help!
The hell's going on?
The sink...
And the blood... It's...
What blood?
The sink. You don't see it?
<i>There was blood.</i>
You worry me, Bevvie.
<i>You worry me a lot.</i>
But don't you see?
Why'd you do this to your hair?
Makes you look like a boy.
Georgie.
<i>I lost it, Billy. Don't be mad.</i>
I'm not mad at you.
It just floated off.
But, Bill, if you'll come with me, you'll float, too.
Georgie.
You'll float, too. You'll float, too.
<i>You'll float, too.</i>
- You'll float, too.
- You'll float, too.
<i>You'll float, too. You'll float, too.</i>
<i>You'll float, too.</i>
You'll float, too! You'll float, too!
You'll float, too!
You'll float, too!
<i>No, we gotta go through the alleyway.</i>
- <i>The alley takes way too long.</i>
- <i>No, the alley is so much faster.</i>
<i>The alley is more dangerous and it's disgusting.</i>
- <i>How is it more dangerous?</i>
- <i>It smells like piss and it's gross.</i>
- <i>Just take the side streets for once.</i>
- <i>Oh, my God.</i>
<i>The side streets are the same.
They smell like piss and shit.</i>
<i>Okay, okay. Can you just tell me what she said exactly?</i>
<i>She didn't say anything.</i>
<i>She just said that you guys need to hurry over.</i>
She didn't say anything. Okay. Okay.
You made it. I...
I need to show you something.
What is it?
More than we saw at the quarry?
Shut up! Just shut up, Richie.
My dad will kill me if he finds out
I had boys in the apartment.
Then we'll leave a lookout.
Richie, stay here.
Whoa, whoa, whoa! What if her dad comes back?
Do what you always do. Start talking.
It is a gift.
In there.
<i>What is it?</i>
<i>You'll see.</i>
Are you taking us to your bathroom?
I just want you to know that 89% of the worst accidents in homes are caused in bathrooms.
And, I mean, that's where all the bacteria and fungi are and it's not a really sanitary place...
<i>I knew it!</i>
<i>You see it?</i>
- <i>Yes.</i>
- <i>What...</i>
<i>What happened in here?</i>
My dad couldn't see it.
I thought I might be crazy.
<i>Well, if you're crazy, then we're all crazy.</i>
We can't leave it like this.
<i>All right, so you've</i>
<i>never been to the Derry Summer Fair?</i>
No, I don't think so.
<i>Not that I know of.</i>
<i>Well, I go there every year,</i>
<i>but I was there one time with Richie and technically won 'cause I hit the target.</i>
<i>But there were so many prizes,
I didn't know which to pick.</i>
It's not true, you know.
What they say about me.
I was only ever kissed by one guy.
It was a long time ago.
It was a nice kiss though.
"January embers."
Was that in the play?
No, the poem.
Oh...
Oh, I don't really know much poetry.
Oh. I was just...
Never mind then.
Um...
Just so you know, I...
I never believed any of the rumors.
And none of us Losers do.
We like hanging with you.
Thanks.
You shouldn't thank us too much.
Hanging out with us makes you a Loser, too.
I can take that.
No, I love being your personal doorman, really.
Could you idiots have taken any longer?
- All right, shut up, Richie.
- Yeah, shut up, Richie.
Oh, okay, trash the trashmouth, I get it.
Hey, I wasn't the one scrubbing the bathroom floor and imagining that her sink went all
Eddie's mom's vagina on Halloween.
She didn't imagine it.
I...
I saw something, too.
You saw blood, too?
Not blood.
I saw Georgie.
It seemed so real.
I mean, it seemed like him, but there was this...
The clown.
Yeah, I saw him, too.
Wait, can only virgins see this stuff?
Is that why I'm not seeing this shit?
<i>Oh, shit, that's Belch Huggins' car.</i>
- We should probably get outta here.
- Yeah.
Wait, isn't that the homeschooled kid's bike?
<i>Yeah, that's Mike's.</i>
We have to help him.
We should?
Yes.
<i>Come on.</i>
<i>Eat that meat!</i>
- Eat it, bitch!
- You little fucker!
- Bitch!
- Motherfucker!
- <i>Eat it, you little bitch!</i>
- <i>Pussy!</i>
<i>Fucking...</i>
<i>Whatcha gonna do, huh?</i>
Get up!
Get the fuck up!
You little fucker!
Come on, Henry, smack him!
You little bitch.
- Nice throw.
- Thanks.
<i>You losers are trying too hard.</i>
She'll do you.
<i>You just gotta ask nicely,</i> like I did.
What the fuck?
<i>Come on, get 'em! Fuck!</i>
Rock war!
- Get 'em!
- Watch out!
Fuck you, motherfuckers!
Eddie!
- <i>Fuck outta here!</i>
- Ow!
Ah, shit!
Fuck you, bitch!
Come on, guys! Let's get 'em!
- Fuck this.
- <i>Fuck out of here.</i>
<i>Fuck you, losers!</i>
Go blow your dad, you mullet-wearing asshole!
Thanks, guys, but you shouldn't have done that.
They'll be after you guys, too, now.
Oh, no, no, no. Bowers? He's always after us.
I guess that's one thing we all have in common.
Yeah, Homeschool.
Welcome to the Losers' Club.
<i>Right there.</i>
<i>They say they found part of his hand all chewed up near the Standpipe.</i>
He asked to borrow a pencil once.
It's like she's been forgotten because Corcoran's missing.
Is it ever gonna end?
What the fuck, dude?
What are you guys talking about?
What they always talk about.
I actually think it will end.
For a little while, at least.
What do you mean?
So I was going over all my Derry research and I charted out all the big events.
The Ironworks explosion in 1908, the Bradley Gang in '35, and The Black Spot in '62.
And now kids being...
I realized this stuff seems to happen...
Every 27 years.
<i>Okay, so, let me get this straight.</i>
<i>It comes out from wherever to eat kids for, like, a year?</i>
<i>And then what? It just goes into hibernation?</i>
Maybe it's like... What do you call it?
Cicadas.
You know, the bugs that come out once every 17 years.
My grandfather thinks this town is cursed.
He says that all the bad things that happen in this town are because of one thing.
An evil thing that feeds off the people of Derry.
But it can't be one thing.
We all saw something different.
<i>Maybe.</i>
Or maybe It knows what scares us most and that's what we see.
I saw a leper.
He was like a walking infection.
But you didn't.
Because it isn't real.
<i>None of this is.</i>
Not Eddie's leper or Bill seeing Georgie or the woman I keep seeing.
She hot?
No, Richie.
She's not hot.
Her face is all messed up.
<i>None of this makes any sense.</i>
They're all like bad dreams.
I don't think so. I know the difference between a bad dream and real life, okay?
What'd you see? You saw something, too?
<i>Yes.</i>
Do you guys know that burned-down house on Harris Avenue?
I was inside when it burned down.
Before I was rescued, my mom and dad were trapped in the next room over from me.
They were pushing and pounding on the door, trying to get to me.
<i>Hurry, son!</i>
<i>It burns!</i>
But it was too hot.
When the firemen finally found them, the skin on their hands had melted down to the bone.
We're all afraid of something.
Got that right.
<i>Why, Rich? What are you afraid of?</i>
Clowns.
Okay.
Look.
<i>That's where Georgie disappeared.</i>
<i>There's the Ironworks.</i>
<i>And The Black Spot.</i>
Everywhere it happens, it's all connected by the sewers.
And they all meet up at the...
The well house.
It's in the house on Neibolt Street.
You mean that creepy-ass house where all the junkies and hobos like to sleep?
<i>I hate that place.</i>
It always feels like it's watching me.
<i>That's where I saw It.</i>
That's where I saw the clown.
That's where It lives.
<i>I can't imagine anything ever wanting to live there.</i>
Can we stop talking about this?
I can barely breathe.
<i>This is summer.</i>
We're kids. I can barely breathe.
I'm up here having a fucking asthma attack.
I'm not doing this.
What the hell? Put the map back.
Mmm-mmm.
What happened?
What's going on?
I got it. Hold on.
Guys.
Georgie.
<i>Bill?</i>
<i>What the fuck?</i>
<i>It's It!</i>
What the fuck is that? What the fuck is that?
I don't fucking know!
Turn it off! Turn it off!
Yeah. Yeah, turn it off. Turn it off!
<i>Run, Stanley!</i>
What is it?
<i>What the fuck?</i>
Thanks, Ben.
It saw us.
It saw us, and it knows where we are.
It always did.
So, let's go.
Go?
Go where?
Neibolt.
That's where Georgie is.
After that?
Yeah, it's summer. We should be outside.
If you say it's summer one more fucking time...
<i>Bill!</i>
Wait!
"He thrusts his fists
"against the posts
"and still insists he sees..."
Bill!
Bill, you can't go in there.
This is crazy.
Look, you don't have to come in with me.
But what happens when another Georgie goes missing?
Or another Betty?
Or another Ed Corcoran? Or one of us?
Are you just gonna pretend it isn't happening like everyone else in this town?
Because I can't.
I go home, and all I see is that Georgie isn't there.
His clothes, his toys, his stupid stuffed animals, but...
<i>He isn't.</i>
So walking into this house, for me, it's easier than walking into my own.
<i>Wow.</i>
<i>What?</i>
He didn't stutter once.
Wait!
Um...
Shouldn't we have some people keep watch?
You know, just in case something bad happens?
Who wants to stay out here?
Fuck.
I can't believe I pulled the short straw.
<i>You guys are lucky you're not measuring dicks.</i>
<i>Shut up, Richie.</i>
<i>I can smell it.</i>
Don't breathe through your mouth.
How come?
Because then you're eating it.
What?
It... It says I'm missing.
You're not missing, Richie.
"Police department, City of Derry."
That's my shirt. That's my hair.
That's my face.
Calm down, this isn't real.
That's my name. That's my age!
That's the date!
It can't be real, Richie.
No, it says it! What the fuck?
- Am I missing? Am I gonna go missing?
- Calm down. Calm down.
Look at me, Richie. Look at me.
That... That isn't real.
It's playing tricks on you.
<i>Hello?</i>
<i>Hello?</i>
<i>Help me, please!</i>
Betty?
Ripsom?
<i>Eddie...</i>
<i>What are you looking for?</i>
Guys, can you hear that?
<i>She was just here. Where the fuck did she go?</i>
Guys.
<i>Oh, my God.</i>
Guys. Guys!
- <i>Guys! Guys!</i>
- What? Eddie?
What the fuck?
Time to take your pill, Eddie.
- Eddie. Open the door!
- <i>Richie.</i>
<i>You okay?</i>
<i>Eddie, what's going on?</i>
Eds.
<i>Eddie.</i>
<i>Come here, Richie.</i>
Eddie.
<i>Eddie.</i>
Eddie.
Where the fuck are you?
<i>We're not playing hide-and-seek, dipshit.</i>
Richie?
Richie! Richie!
Bill, come on, open the door.
<i>It won't open!</i>
- <i>What's going on? Richie!</i>
- I can't!
<i>Open the door, Rich.</i>
<i>Oh, shit.</i>
Stupid clowns.
<i>Oh, fuck.</i>
<i>Ugh!</i>
Beep-beep, Richie.
Let's get outta here.
You wanna play loogie?
Time to float.
<i>Where's my shoe?</i>
Where the fuck were her legs?
Holy shit, what the fuck was that?
This isn't real.
Remember the missing kid poster.
That wasn't real, so this isn't real.
Tasty, tasty, beautiful fear.
Come on. Ready?
<i>No!</i>
No.
No, no!
Oh, thank fuck.
- <i>Where's Eddie?</i>
- <i>Help!</i>
<i>Help!</i>
Eddie!
<i>Holy fuck.</i>
Eddie!
This isn't real enough for you, Billy?
I'm not real enough for you?
Oh, shit.
It was real enough for Georgie.
Holy shit!
- Get Eddie!
- Get Eddie!
Oh, fuck! We gotta get out of here!
- <i>Get Eddie. Let's go!</i>
- <i>Guys, watch out!</i>
- No, no, no, no!
- Eddie, look at me!
He's gonna get us! Guys!
<i>No!</i>
<i>Ben!</i>
Ben!
<i>Let's get out of here!</i>
Don't let him get away!
<i>Bill, we have to help Eddie!</i>
No! No!
I'm gonna snap your arm into place.
Rich, do not fucking touch me.
- Okay, one, two, three.
- Do not touch me!
Holy shit!
You. You did this.
You know how delicate he is.
- We were attacked, Mrs. K.
- No. Don't.
Don't try and blame anyone else.
- Let me help.
- Get back!
Oh, I've heard of you, Miss Marsh.
And I don't want a dirty girl like you touching my son.
Mrs. K, I...
No! You are all monsters. All of you.
And Eddie is done with you.
Do you hear? Done!
I saw the well.
We know where it is and next time we'll be better prepared.
No!
No next time, Bill.
- You're insane.
- <i>Why?</i>
We all know no one else is going to do anything.
Eddie was nearly killed!
And look at this motherfucker.
He's leaking Hamburger Helper!
<i>We can't pretend it's gonna go away.</i>
Ben, you said it yourself,
It comes back every 27 years.
Fine! I'll be 40 and far away from here.
I thought you said you wanted to get out of this town, too.
Because I wanna run towards something.
Not away.
I'm sorry, who invited Molly Ringwald into the group?
- Richie...
- I'm just saying, let's face facts.
Real world.
Georgie is dead.
Stop trying to get us killed, too.
Georgie's not dead.
You couldn't save him, but you can still save yourself.
No, take it back.
You're scared and we all are, but take it back!
Bill!
You're just a bunch of losers!
- Fuck off!
- Richie, stop.
You're just a bunch of losers and you'll get yourselves killed
- trying to catch a fucking stupid clown.
- Stop!
This is what It wants.
It wants to divide us.
We were all together when we hurt It.
That's why we're still alive.
Yeah? Well, I plan to keep it that way.
- Mike...
- Guys...
I can't do this.
My granddad was right.
I'm an outsider. Gotta stay that way.
- Yeah! Bonus!
- Yeah, there we go.
Put the next target out there.
<i>Just hold it.</i>
What the hell's going on here?
Just cleaning your gun, like you asked.
You're cleaning my gun, huh?
- Dad...
- Hey!
Look at him now, boys.
Ain't nothing like a little fear to make a paper man crumble.
<i>Here for the refills, Eddie?</i>
Yeah.
<i>You know it's all bullshit, right?</i>
What is?
Your medication.
<i>They're placebos.</i>
What's placebo mean?
Placebo means bullshit.
No friends, huh?
<i>Your cast.</i>
No signatures or anything?
So sad.
I didn't want it to get dirty.
I'll sign it for you.
You okay, Henry?
<i>And this is my most</i>
<i>favorite part of the afternoon.</i>
<i>Getting to know all about so many of you.</i>
<i>Is there someone here that wants to share with us</i>
- <i>what they most enjoyed about today?</i>
- <i>Me!</i>
- <i>How about you?</i>
- <i>I liked seeing the clown.</i>
- <i>You did? You liked the clown?</i>
- <i>Yes!</i>
- <i>What about the rest of you?</i>
- <i>I liked when the bubbles float.</i>
<i>You did? Me too.</i>
<i>I just love watching things float.</i>
<i>We all float.</i>
<i>That's right.</i>
<i>And you will, too, Henry.</i>
<i>Make it a wonderful day.</i>
<i>Kill him.</i>
<i>Kill him. Kill him.</i>
<i>Kill him. Kill him.</i>
<i>Kill him. Kill him.</i>
<i>Oh, no. Give a big round of applause!</i>
<i>Well done, Henry.</i>
<i>Kill them all. Kill them all.</i>
<i>Kill them all!</i>
<i>Kill them all! Kill them all! Kill them all!</i>
<i>Kill them all! Kill them all! Kill them all!</i>
<i>Where are you sneaking off to?</i>
Nowhere, Daddy.
You're looking prettied up.
I'm not prettied up, Daddy.
I wear this almost every day.
Come.
<i>You know I worry about you, Bevvie.</i>
I know.
People in town have been saying some things to me about you.
Sneaking around all summer long with a bunch of boys.
<i>The only girl in the pack.</i>
They're just friends, I swear.
I know what's in boys' minds when they look at you, Bevvie.
I know all too well.
My hand...
Are you doing womanly things down in the woods with those boys?
No, no, no, nothing.
You don't have to worry. I promise.
What's this?
It's nothing. It's just a poem.
Just a poem?
But you had to hide it in your underwear drawer.
Why would you have to hide it there?
Are you still my girl?
No.
What did you say?
I said, no!
No!
Get away, no!
Get off!
No!
Those boys. Do they know that you're my...
Beverly?
Beverly!
- Richie.
- What do you want?
<i>See that guy I'm hitting?
I'm pretending it's you.</i>
It got Beverly.
What are you talking about?
It, Richie.
It got Beverly.
Hello?
Okay.
I'll meet you there.
And just where do you think you're off to?
Out with my friends.
Sweetie, you can't go.
You're getting over your sickness, remember?
My sickness?
Okay, what sickness, Ma?
You know what these are? They're gazebos!
They're bullshit!
They help you, Eddie.
I had to protect you.
Protect me?
By lying to me? By keeping me locked inside this hellhole?
I'm sorry but the only people that were actually trying to protect me were my friends.
And you made me turn my back on them when I really needed them.
So I'm going.
Eddie. Eddie. No.
You get back here.
- Sorry, Mom, I gotta go save my friends.
- Eddie!
Don't do this to me, Eddie!
Guys, spikes.
Stan?
Stan, we all have to go.
<i>Beverly was right.</i>
If we split up like last time, that clown will kill us one by one.
<i>But if we stick together,</i> all of us, we'll win.
<i>I promise.</i>
Hey, Eddie, you got a quarter?
I wouldn't want to make a wish in that fucking thing.
Beverly?
How are we supposed to get down there?
All right. Come on.
All right.
Guys, guys. Help.
- All right, buddy?
- Yeah, I'm okay.
<i>Step right up, Beverly. Step right up.</i>
<i>Come change. Come float.
You'll laugh. You'll cry.</i>
<i>You'll cheer. You'll die.</i>
<i>Introducing Pennywise the Dancing Clown.</i>
I'm not afraid of you.
You will be.
<i>Help! Help me!</i>
Ahhh!
Die!
Mike!
- Mike!
- Mike!
<i>You okay? Mike!</i>
<i>Bowers.</i>
Mike. Fuck.
- <i>Mike!</i>
- <i>Where is he?</i>
<i>We're next.</i>
<i>No, no, no!</i>
- Grab it!
- Get the rope! Get the rope!
Oh, shit.
- <i>Mike!</i>
- <i>Mike!</i>
<i>Leave him alone!</i>
You didn't listen to what I told you, did you?
You should've stayed out of Derry.
Your parents didn't and look what happened to them.
I still get sad every time
I pass by that pile of ashes.
Sad, that I couldn't have done it myself.
<i>Run, Mike!</i>
Mike!
Fuck down!
I should get up there.
Are you insane? With what?
<i>Holy shit.</i>
- Oh, my...
- Mike!
I'm okay.
<i>I'm okay.</i>
Shoot!
<i>Stanley.</i>
<i>Beverly?</i>
<i>Is that you?</i>
Guys?
Guys?
- What?
- Guys, where's Stan?
Stanley!
Stanley!
Stan!
Oh, shit. Greywater.
Stan?
Stan?
Stanley!
- Stan!
- Shit! Stan!
Stanley!
- We're coming, man!
- We're coming! Don't worry.
- Stan?
- Stanley! Stan!
His flashlight!
<i>What the fuck is that thing?</i>
Oh, shit.
<i>Oh, shit.</i>
Oh, shit!
<i>Oh, shit!</i>
Stanley!
- Stanley! Stan!
- Stanley!
No! No! No!
It's okay.
You left me! You took me into Neibolt!
You're not my friends!
You made me go into Neibolt!
- <i>Stanley, I'm sorry.</i>
- <i>You made me go into Neibolt!</i>
<i>This is your fault.</i>
<i>We would never let anything happen to you.</i>
<i>We're here for you.</i>
You know we wouldn't do that to you. Come on.
Bill! Bill!
Bill!
<i>Bill!</i>
Beverly!
I'll come back for you, Bev.
Bill!
Bill!
Bill!
<i>Come on.</i>
Get out of there, dude. That's greywater.
Wait, wait, wait.
Oh, my God, my fucking flashlight?
Eddie!
Come on, let's get the fuck out of here! Come on!
- Holy shit.
- Bev? Bev?
- Beverly?
- Holy shit.
Bev.
<i>How is she in the air?</i>
Guys.
Are those...
<i>The missing kids.</i>
<i>Floating.</i>
- Just let me grab her.
- Shit.
<i>I'm slipping.</i>
Bev.
Beverly.
Why isn't she waking up?
What is wrong with her?
Beverly, please! Come on!
Wow.
<i>Bev?</i>
"January embers"
"My heart burns there too"
Jesus, fuck.
Oh, God.
Where's Bill?
<i>Georgie.</i>
What took you so long?
I was looking for you this whole time.
I couldn't find my way outta here.
He said I could have my boat back, Billy.
Was she fast?
I couldn't keep up with it.
"She," Georgie.
You call boats "she."
Take me home, Billy.
I wanna go home.
<i>I miss you. I wanna be with Mom and Dad.</i>
I want more than anything for you to be home.
With Mom and Dad.
I miss you so much.
I love you, Billy.
I love you, too.
But you're not Georgie.
- Kill It, Bill! Kill It!
- <i>Shit.</i>
<i>Kill It, Bill!</i>
Kill It! Bill, Kill It!
<i>Kill It!</i>
- Kill It!
- Kill!
Kill It!
It's not loaded.
Do it now, Bill! Kill It! Kill It!
Hey! It's not loaded!
<i>Holy fuck.</i>
Oh, shit.
<i>Bill, watch out!</i>
<i>Leave him alone!</i>
<i>Beverly, no!</i>
<i>Mike!</i>
<i>Help him!</i>
Fuck!
Ben!
<i>Stanley!</i>
Bill!
<i>Bill!</i>
<i>No, don't.</i>
Let him go.
No.
I'll take him.
I'll take all of you.
<i>And I'll feast on your flesh as I feed on your fear.</i>
Or, you'll just leave us be.
I will take him, only him and then I will have my long rest and you will all live to grow and thrive and lead happy lives until old age takes you back to the weeds.
Leave...
I'm the one who dragged you all into this.
I'm so sorry.
<i>Sorry.</i>
Go!
Guys, we can't!
<i>I'm sorry.</i>
I told you, Bill.
<i>I fucking told you.</i>
<i>I don't want to die.</i>
It's your fault.
You punched me in the face.
You made me walk through shitty water.
You brought me to a fucking crackhead house.
And now...
I'm gonna have to kill this fucking clown.
Welcome to the Losers' Club, asshole!
- Mike!
- Mike!
<i>Stan, watch out!</i>
Mike!
<i>Kill him!</i>
I'm gonna kill you!
<i>Hey, Bevvie. Are you still my little...</i>
<i>Oh, shit!</i>
That's why you didn't kill Beverly.
'Cause she wasn't afraid.
<i>And we aren't either.</i>
<i>Not anymore.</i>
Now you're the one who's afraid.
Because you're gonna starve.
<i>"He thrusts his fists against the posts</i>
<i>"and still insists he sees the ghosts</i>
<i>"He thrusts his fists against the posts"</i>
Fear...
I know what I'm doing for my summer experience essay.
Guys. Guys.
The kids are floating down.
I can only remember parts,
I thought I was dead.
That's what it felt like.
I saw us, all of us together back in the cistern, but we were older, like, our parents' ages.
What were we all doing there?
I just remember how we felt.
<i>How scared we were.</i>
I don't think I can ever forget that.
Swear it.
Swear if It isn't dead, if It ever comes back, we'll come back, too.
<i>I gotta go.</i>
I hate you.
I'll see you later.
<i>Bye, Stan.</i>
Bye, guys.
- Bye, Mike.
- See you later, losers.
- See you around, Bill.
- See you, Rich.
See you guys later.
Bye, Ben.
You all packed for Portland?
Yeah, pretty much.
I'm going tomorrow morning.
How long will you be gone?
My aunt, she says I can stay for as long as I want, so...
Just so you know...
I never felt like a loser when I was with all of you.
See you around.
Bye.
